import xml.etree.ElementTree as ET

a_file = open("trackhub.txt","r")

texttolists = []
for line in a_file:
        stripped_line = line.strip()
        line_list = stripped_line.split()
        texttolists.append(line_list)

a_file.close()

print(texttolists)


def create_annots_xml():
    # we make root element
    files = ET.Element("files")

    # create sub element
    files = ET.SubElement(files, "files")

    # insert list element into sub elements
    for line in texttolists:
        if("bigDataUrl" in line):
            filename = ET.SubElement(files, "file name")
            filename.text = line[1]

    tree = ET.ElementTree(files)

    # write the tree into an XML file
    tree.write("annots.xml", encoding='utf-8', xml_declaration=True)


create_annots_xml()







































# f = open('annots.xml', 'w')
# f.write('<?xml version="1.0" encoding="UTF-8"?>')
#
# for person in persons_list :
#     f.write('<person>')
#     f.write('<name>' + person[0] + '</name>')
#     f.write('<age>' + person[1] + '</age>')
#     f.write('<color> ' + person[2] + '</color>')
#     f.write('</person>')
#
# f.write('</persons>')
# f.close()