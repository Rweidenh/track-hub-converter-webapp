import requests
import json
from enum import Enum
from xml.dom import minidom
import re

class Data_Type(Enum):
    CONTENTS = 1
    TRACKDB = 2

def get_hubGenomes_txt(hub_link, data_type, genome_name = None):
    url = "https://api.genome.ucsc.edu/list/hubGenomes?hubUrl="+hub_link
    res = requests.get(url)
    result = ""
    if res:
        res_json = json.loads(res.text)
        res_genomes = res_json["genomes"]
        for x in res_genomes:
            if data_type == Data_Type.CONTENTS:
                result += str(x) +"\t"+ str(res_genomes[x]['description'])+"\n"
            elif data_type == Data_Type.TRACKDB and x == genome_name:
                return res_genomes[x]['trackDbFile']
    else:
        result = "404 error"
    return result

def get_track_db_loc(hub_link, genome_name):
    return get_hubGenomes_txt(hub_link, Data_Type.TRACKDB, genome_name)

def create_annots_xml(hub_link, genome_name):
    track_db_loc = get_track_db_loc(hub_link, genome_name)
    full_path = '/'.join(track_db_loc.split('/')[:-1]) + "/"
    res = requests.get(track_db_loc)
    result = ""
    if res:
        content = res.text.split("\n")
        # create file
        root = minidom.Document()
        # creat root element
        xml = root.createElement('files')
        root.appendChild(xml)
        trackName = ""
        for line in content:
            if "track" in line:
                line = re.sub(' +', ' ', line)
                line = line.strip()
                trackName = line.split(" ")[1]
            if "bigDataUrl" in line:
                line = re.sub(' +', ' ', line)
                line = line.strip()
                bigDataUrl = line.split(" ")[1]

                bigDataUrl = bigDataUrl if "http" in bigDataUrl else full_path+bigDataUrl
                # create child element
                filesChild = root.createElement('file')
                filesChild.setAttribute('name', bigDataUrl)
                filesChild.setAttribute('title', trackName)
                xml.appendChild(filesChild)
        xml_str = root.toprettyxml(indent ="\t")
        return xml_str
    return "404 error"

def create_contents_txt(hub_link):
    return get_hubGenomes_txt(hub_link, Data_Type.CONTENTS)


def create_genome_txt(hub_link, genome, assembly):
    url = "https://api.genome.ucsc.edu/list/chromosomes?hubUrl="+hub_link+";genome={};track={}".format(genome, assembly)
    res = requests.get(url)
    result = ""
    if res:
        res_json = json.loads(res.text)
        res_genomes = res_json["chromosomes"]

        for x in res_genomes:
            result += str(x) +"\t"+ str(res_genomes[x])+"\n"
    else:
        result = "404 error"
    return result

if __name__ == '__main__':
    contents = create_contents_txt("http://hgdownload.soe.ucsc.edu/hubs/mouseStrains/hub.txt")
    chromosomes = create_genome_txt("http://hgdownload.soe.ucsc.edu/hubs/mouseStrains/hub.txt", "CAST_EiJ", "assembly")
    annots = create_annots_xml("http://hgdownload.soe.ucsc.edu/hubs/mouseStrains/hub.txt", "CAST_EiJ")
    print(contents)
    print("---------------------------------------------")
    print("---------------------------------------------")
    print(chromosomes)
    print("---------------------------------------------")
    print("---------------------------------------------")
    print(annots)
